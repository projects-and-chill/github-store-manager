ifeq (create,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (delete,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

REPO_LOCATION = <repo-owner>/<repo-name>

init :
	git init
	git add --all 
	git commit -m"first commit"
	git remote add origin "git@github.com:$(REPO_LOCATION).git"
	git push --all
	
.PHONY: create
create :
	git checkout --orphan $(RUN_ARGS)
	git rm  -rf .
	touch README.md
	git add --all
	git commit -m "first commit on $(RUN_ARGS)"
	git push origin $(RUN_ARGS):$(RUN_ARGS)
	git checkout master
	echo "- [$(RUN_ARGS)](https://github.com/$(REPO_LOCATION)/tree/$(RUN_ARGS))" >> README.md
	git commit -a -m "add new branch named '$(RUN_ARGS)'"
	git push origin master
	git checkout $(RUN_ARGS)
	
.PHONY: delete
delete:
	git checkout master
	grep -v "https://github.com/$(REPO_LOCATION)/tree/$(RUN_ARGS)" README.md > tmp && mv tmp README.md
	git branch -D $(RUN_ARGS)
	git commit -a -m "delete branch named '$(RUN_ARGS)'"
	git push origin --delete $(RUN_ARGS)
	git push origin master
