# Init store:

**1-** Before all : delete `.git` directory !

**2-** Edit `./Makefile` and insert repo informations ( `repo_owner/repo_name` ) in **REPO_LOCATION** variable, ex: 

```Makefile
#./Makefile
REPO_LOCATION := enzo-cora/bookShelves
```
**3-** Then execute this command :
```cmd
	$ make init
```
# Manage  store:
- For create new empty branch (**localy** and **remotly**)
```cmd
$ make create <newBranch>
```

- For delete existing  branch (**localy** and **remotly**)

```cmd
$ make delete <existingBranch>
```

# This store contain: 

This section will automatically populate when executing the `make create <branch>` and `make delete <branch>` commands.
- [docker-modele](#)
- [aws-infrastructure](#)
- [...](#)
